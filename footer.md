<div markdown="1" class="columns medium-6">

  # About the Founder's Playbook

  The Founder's Playbook was developed by Cyclotron Road to be the go-to knowledge
  repository for hard tech innovators and entrepreneurs.

</div>

<div markdown="1" class="columns medium-6">

  # Join the Community

  The Founder's Playbook was developed by Cyclotron Road to be the go-to knowledge
  repository for hard tech innovators and entrepreneurs.

</div>

---

<div markdown="1" class="columns">

  # The Founder's Playbook Community

  * Cyclotron Road
  * MH
  * Stanford University
  * MIT
  * Breakthrough Energy

</div>